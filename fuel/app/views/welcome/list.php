一覧表示
<style>
    td{
        padding:0 30px;
        text-align: center;
    }
    img{
        width:400px;
        height: 300px;
    }
    .all{
        width:80%;
        margin:0 auto;
    }
</style>
<div class="all">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            星座名：<input type="text" name="星座名" value="<?= $星座名; ?>"><br>   
            学名：<input type="text" name="学名" value="<?= $学名; ?>"><br>
            <input type="file" name="image">
            <?php if($is_update) : ?>
         <input type="hidden" name="id" value="<?= $id ?>">
         <input type="submit" name="save" value="更新">
<?php else : ?>
         <input type="submit" name="insert" value="登録">
<?php endif; ?>
        </form>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>星座名</th>
                <th>学名</th>
                <th>画像</th>
<!--                <th>赤経</th>
                <th>赤緯</th>-->
            </tr>
            <?php foreach ($星座一覧 as $zodiac) : ?>
            <tr>
                <td><?php echo $zodiac['id']; ?></td>
                <td><?php echo $zodiac['星座名']; ?></td>
                <td><?php echo $zodiac['学名']; ?></td>
                <td><img src="<?php echo Uri::base() . $zodiac['画像']; ?>"></td>
<!--                <td><?php echo $zodiac['赤経']; ?></td>
                <td><?php echo $zodiac['赤緯']; ?></td>-->
                <td>
                    <form action="<?php echo Uri::create('welcome/list'); ?>" method="post">
                        <input type="hidden" name="update_id" value="<?= $zodiac['id'] ?>">
                        <input type="submit" name="update" value="編集">
                    </form>  
                    <form action="<?php echo Uri::create('welcome/list/delete'); ?>" method="post">
                        <input type="hidden" name="id" value="<?= $zodiac['id'] ?>">
                        <input type="submit" name="delete" value="削除" 
                               onclick="if(confirm('削除してよろしいですか？')){
                                   return true;}else{return false;}">
                    </form>
                </td>
            </tr>
           <?php endforeach; ?>
        </table>
</div>
<?php
//var_dump($星座一覧);
