<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
        public function action_list()
        {
            $data = array();
            $data['action'] = Uri::create('welcome/insert');//通常の場合
            $data['is_update'] = false;//編集の場合
            $data['星座名'] = '';
            $data['学名'] = '';
            $data['画像'] = '';            
            
            
            //更新ボタンに変わる
            $update_id = Input::post('update_id',0);
            if ($update_id){
                $data['action'] = Uri::create('welcome/update');//編集の場合
                $data['is_update'] = true;//編集の場合
                $data['id'] = $update_id;

                //SELECT * FROM `items` WHERE `id` = ?
    //            $query = DB::select()->from('items')->where('id',$update_id);
    //            $result = $query->execute();
    //            $item = $result[0];
                $model = Model_Zodiac::find($update_id);
                $data['星座名'] = $model['星座名'];
                $data['学名'] = $model['学名'];
                $data['画像'] = $model['画像'];
            }

            $data['星座一覧'] = Model_Zodiac::find('all');
            return Response::forge(View::forge('welcome/list',$data));
        }

                public function get_upload()
        {
            $data = array();
            return Response::forge(View::forge('welcome/update', $data));
        }
    
        public function post_insert()
        {
            $image = Input::file('image');
            if ($image['name'] != "") {
                //var_dump($image);
                // 画像の保存
                move_uploaded_file($image['tmp_name'], 'assets/img/'.$image['name']);
                
                // DBへの保存
                $model = Model_Zodiac::forge();
                $model->星座名 = Input::post('星座名','');
                $model->学名 = Input::post('学名','');;
                $model->画像 = 'assets/img/'.$image['name'];
                $model->赤経 = '';
                $model->赤緯 = '';
                $model->save();
                
                Response::redirect('welcome/list');
            } else {
                Response::redirect('welcome/list');
            }
        }
        
            //編集の場合

        public function post_update(){
          $id = Input::post('id','');
          $model = Model_Zodiac::find($id);
          $model->星座名 = Input::post('星座名','');
          $model->学名 = Input::post('学名','');
          $image = Input::file('image');
          if ($image['name'] != "") {
            // 画像の保存
            move_uploaded_file($image['tmp_name'], 'assets/img/'.$image['name']);
            $model->画像 = 'assets/img/'.$image['name'];
          }

          $model-> save();
          
          Session::set_flash(
                  'success','id:'.$id. 'のデータを更新しました。');
          return Response::redirect('welcome/list');
      }
      
        public function post_delete(){
            $id = Input::post('id',0);
            //DELETE FROM `items` WHERE `id` = ?
  //          $query = DB::delete('items')->where('id',$id);
  //          $query->execute();
            $item = Model_Zodiac::find($id);
  //        $item ->delete();
            $model -> delete_datetime = date('Y-M-d H:i*s');
            $item -> save();

            Session::set_flash(
                    'success','id:'.$id. 'のデータを削除しました。');
            return Response::redirect('welcome/list');
        }
      
       /**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		return Response::forge(View::forge('welcome/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
